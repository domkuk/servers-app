import React from 'react';
import renderer from 'react-test-renderer';
import {ServersTable, Props} from './ServersTable';

describe('ServersTable', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      onFetchServers: () => {},
      servers: [{name: 'Test name', distance: 1000}],
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = (props: Props) => (
  <ServersTable {...props}/>
);
