import React, {useEffect, useState} from 'react';
import Table from '../../../common/Table/Table';
import styles from './ServersTable.module.scss';
import {useTable, Table as TableType} from '../../../hooks/useTable/useTable';
import {Server} from '../../../domain/Server';
import _ from 'lodash';
import {StoreState} from '../../../config/StoreProvider/StoreProvider';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import * as serverService from '../../../store/server/service';
import {connect} from 'react-redux';
import Loader from '../../../common/Loader/Loader';

const HEADERS = [
  {
    label: 'Name',
    sortBy: 'name',
  },
  {
    label: 'Distance',
    sortBy: 'distance',
  },
];

export type Props = {
  onFetchServers: () => void,
  isLoading: boolean,
  servers: Array<Server>
}

export const ServersTable = ({onFetchServers, isLoading, servers}: Props) => {
  const [filteredServers, setFilteredServers] =
    useState<Array<Server>>(servers);

  useEffect(() => {
    onFetchServers();
  }, [onFetchServers]);

  useEffect(() => {
    setFilteredServers((prevState) =>
      tableParams.sortBy ?
        _.orderBy(
          prevState,
          [tableParams.sortBy],
          [tableParams.sortDirection],
        ) :
        servers,
    );
  }, [servers, setFilteredServers]);

  const handleUpdate = (tableParams: TableType) => {
    setFilteredServers((prevState) =>
      tableParams.sortBy ?
        _.orderBy(
          prevState,
          [tableParams.sortBy],
          [tableParams.sortDirection],
        ) :
        servers,
    );
  };

  const {tableParams, onSort} = useTable(handleUpdate);

  return (
    <Loader isLoading={isLoading}>
      <Table
        headers={HEADERS}
        className={styles.table}
        onSort={onSort}
        tableParams={tableParams}
      >
        {filteredServers.map((server) => (
          <tr key={server.name + server.distance}>
            <td>{server.name}</td>
            <td>{server.distance}</td>
          </tr>
        ))}
      </Table>
    </Loader>
  );
};

const mapStateToProps = (state: StoreState) => ({
  servers: state.server.servers,
  isLoading: state.server.serversLoading,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
  onFetchServers: () => dispatch(serverService.fetchServers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServersTable);
