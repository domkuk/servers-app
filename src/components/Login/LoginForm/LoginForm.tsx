import React from 'react';
import {connect} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import Form from '../../../common/Form/Form';
import styles from './LoginForm.module.scss';
import {useForm} from '../../../hooks/useForm/useForm';
import {StoreState} from '../../../config/StoreProvider/StoreProvider';
import * as authService from '../../../store/auth/service';
import {LoginRequest} from '../../../store/auth/service';

const INPUTS = [
  {
    name: 'username',
    placeholder: 'Username',
    type: 'text',
    validation: [
      {
        type: 'required',
      },
    ],
  },
  {
    name: 'password',
    placeholder: 'Password',
    type: 'password',
    validation: [
      {
        type: 'required',
      },
    ],
  },
];

type Props = {
  onLoginSubmit: (inputs: LoginRequest) => void,
  isLoading: boolean,
  error: string | null,
};

export const LoginForm = ({onLoginSubmit, isLoading, error}: Props) => {
  const {
    inputs,
    onSubmit,
    onInputChange,
    onLoseInputFocus,
  } = useForm<LoginRequest>(INPUTS, onLoginSubmit);

  return (
    <Form
      inputs={inputs}
      isLoading={isLoading}
      submitButtonChild='Login'
      submitButtonClassName={styles.submitButton}
      onInputChange={onInputChange}
      onLoseInputFocus={onLoseInputFocus}
      onSubmit={onSubmit}
      error={error}
    />
  );
};

const mapStateToProps = (state: StoreState) => ({
  error: state.auth.loginError,
  isLoading: state.auth.loginLoading,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
  onLoginSubmit: (inputs: LoginRequest) => dispatch(authService.login(inputs)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
