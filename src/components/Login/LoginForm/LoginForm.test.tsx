import React from 'react';
import renderer from 'react-test-renderer';
import {LoginForm} from './LoginForm';

describe('LoginForm', () => {
  it('should render component correctly', () => {
    const component = getComponent();
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = () => (
  <LoginForm onLoginSubmit={() => {}} isLoading={false}/>
);
