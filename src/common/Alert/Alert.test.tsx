import React from 'react';
import renderer from 'react-test-renderer';
import Alert, {Props} from './Alert';

describe('Alert', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      className: 'testClass',
      variant: 'danger',
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = (props: Props) => (
  <Alert {...props}>
    Test children
  </Alert>
);
