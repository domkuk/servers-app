import React, {ReactNode} from 'react';
import styles from './Alert.module.scss';
import cn from 'classnames';

export type Props = {
  children?: ReactNode,
  className?: string,
  variant: 'danger',
};

const Alert = ({children, className, variant}: Props) => (
  <div className={cn(styles.alert, styles[variant], className)}>
    {children}
  </div>
);

export default Alert;
