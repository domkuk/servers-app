import React from 'react';
import renderer from 'react-test-renderer';
import Form, {Props} from './Form';
import {shallow} from 'enzyme';

describe('Form', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      inputs: [{name: 'test', type: 'password'}],
      isLoading: false,
      onInputChange: () => {},
      onLoseInputFocus: () => {},
      onSubmit: () => {},
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly with submit button', () => {
    const component = getComponent({
      inputs: [{name: 'test', type: 'password'}],
      onInputChange: () => {},
      onLoseInputFocus: () => {},
      onSubmit: () => {},
      submitButtonChild: 'Test',
      submitButtonClassName: 'testClass',
      isLoading: false,
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly with error message', () => {
    const component = getComponent({
      inputs: [{name: 'test', type: 'password'}],
      onInputChange: () => {},
      onLoseInputFocus: () => {},
      onSubmit: () => {},
      submitButtonChild: 'Test',
      submitButtonClassName: 'testClass',
      isLoading: false,
      error: 'Test error message',
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should call event on form submit', () => {
    const onSubmit = jest.fn();
    const component = shallow(getComponent({
      inputs: [{name: 'test', type: 'password'}],
      onInputChange: () => {},
      onLoseInputFocus: () => {},
      submitButtonChild: 'Test',
      submitButtonClassName: 'testClass',
      isLoading: false,
      onSubmit,
    }));
    component.find('form').simulate('submit');
    expect(onSubmit).toHaveBeenCalled();
  });
});

const getComponent = (props: Props) => (
  <Form {...props}/>
);
