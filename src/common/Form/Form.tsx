import React, {ChangeEvent, ReactNode} from 'react';
import Input, {InputType} from '../Input/Input';
import styles from './Form.module.scss';
import Button from '../Button/Button';
import Alert from '../Alert/Alert';

export type Props = {
  submitButtonChild?: ReactNode,
  submitButtonClassName?: string,
  inputs: Array<InputType>,
  onInputChange: (event: ChangeEvent<HTMLInputElement>) => void,
  onLoseInputFocus: (event: ChangeEvent<HTMLInputElement>) => void,
  onSubmit: (event: ChangeEvent<HTMLFormElement>) => void,
  isLoading: boolean,
  error?: string | null,
};

const Form = ({
  submitButtonChild,
  submitButtonClassName,
  inputs,
  onInputChange,
  onSubmit,
  onLoseInputFocus,
  isLoading,
  error,
}: Props) => (
  <form onSubmit={onSubmit} noValidate>
    {error && (
      <Alert variant='danger' className={styles.alert}>
        {error}
      </Alert>
    )}
    {inputs.map((input) => (
      <Input
        key={input.name}
        className={styles.input}
        onInputChange={onInputChange}
        onLoseFocus={onLoseInputFocus}
        {...input}
      />
    ))}
    {submitButtonChild &&(
      <Button
        type='submit'
        className={submitButtonClassName}
        isLoading={isLoading}
      >
        {submitButtonChild}
      </Button>
    )}
  </form>
);

Form.defaultProps = {
  inputs: [],
  onSubmit: () => {},
  onLoseFocus: () => {},
};

export default Form;
