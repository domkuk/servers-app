import React from 'react';
import renderer from 'react-test-renderer';
import Button, {Props} from './Button';
import {shallow} from 'enzyme';

describe('Button', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      type: 'submit',
      variant: 'primary',
      className: 'testClass',
      isLoading: false,
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly when loading', () => {
    const component = getComponent({
      type: 'submit',
      variant: 'primary',
      className: 'testClass',
      isLoading: true,
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should call event on button click', () => {
    const onClick = jest.fn();
    const component = shallow(getComponent({
      type: 'submit',
      variant: 'primary',
      isLoading: false,
      onClick,
    }));
    component.find('button').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});

const getComponent = (props: Props) => (
  <Button {...props}>
    Test
  </Button>
);
