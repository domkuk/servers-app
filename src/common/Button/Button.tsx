import React, {ReactNode} from 'react';
import styles from './Button.module.scss';
import cn from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCircleNotch} from '@fortawesome/free-solid-svg-icons';

export type Props = {
  children?: ReactNode,
  className?: string,
  onClick?: () => void,
  type: 'button' | 'submit' | 'reset' | undefined,
  variant: 'primary',
  isLoading: boolean,
};

const Button = ({
  children,
  className,
  onClick,
  type,
  variant,
  isLoading,
}: Props) => (
  <button
    type={type}
    className={cn(
      styles.button,
      styles[variant],
      {[styles.disabled]: isLoading},
      className,
    )}
    onClick={onClick}
    disabled={isLoading}
  >
    {isLoading && (
      <FontAwesomeIcon
        className={styles.loadingIcon}
        fixedWidth
        spin
        icon={faCircleNotch}
      />
    )}
    {children}
  </button>
);

Button.defaultProps = {
  variant: 'primary',
  onClick: () => {},
  isLoading: false,
};

export default Button;
