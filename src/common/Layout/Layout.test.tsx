import React from 'react';
import renderer from 'react-test-renderer';
import {MemoryRouter} from 'react-router-dom';
import Layout, {Props} from './Layout';

jest.mock('../Navigation/Navigation', () => 'Navigation');

describe('Layout', () => {
  it('should render component correctly when authenticated', () => {
    const component = getComponent({isAuthenticated: true});
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly when not authenticated', () => {
    const component = getComponent({isAuthenticated: false});
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = (props: Props) => (
  <MemoryRouter>
    <Layout {...props}>
      Test
    </Layout>
  </MemoryRouter>
);
