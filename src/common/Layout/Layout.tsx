import React, {ReactNode} from 'react';
import styles from './Layout.module.scss';
import Navigation from '../Navigation/Navigation';

export type Props = {
  children?: ReactNode,
  isAuthenticated: boolean,
}

const Layout = ({children, isAuthenticated}: Props) => (
  <div className={styles.layout}>
    {isAuthenticated && <Navigation/>}
    {children}
  </div>
);

export default Layout;
