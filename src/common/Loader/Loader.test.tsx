import React from 'react';
import renderer from 'react-test-renderer';
import Loader, {Props} from './Loader';

describe('Loader', () => {
  it('should render component correctly when not loading', () => {
    const component = getComponent({
      isLoading: false,
      isFullScreen: false,
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly when loading', () => {
    const component = getComponent({
      isLoading: true,
      isFullScreen: false,
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly when loading fullscreen', () => {
    const component = getComponent({
      isLoading: true,
      isFullScreen: true,
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = (props: Props) => (
  <Loader {...props}>
    Test children
  </Loader>
);
