import React from 'react';
import renderer from 'react-test-renderer';
import {MemoryRouter} from 'react-router-dom';
import {Navigation, Props} from './Navigation';

describe('Navigation', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      onLogout: () => {},
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = (props: Props) => (
  <MemoryRouter>
    <Navigation {...props}/>
  </MemoryRouter>
);
