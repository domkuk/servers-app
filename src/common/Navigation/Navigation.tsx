import React from 'react';
import styles from './Navigation.module.scss';
import {NavLink} from 'react-router-dom';
import cn from 'classnames';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import {connect} from 'react-redux';
import {logout} from '../../store/auth/actions';

const NAVIGATION_ITEMS = [
  {
    label: 'Servers',
    to: '/servers',
  },
];

export type Props = {
  onLogout: () => void,
};

export const Navigation = ({onLogout}: Props) => (
  <div className={styles.navigation}>
    {NAVIGATION_ITEMS.map((navigationItem) => (
      <NavLink
        key={navigationItem.label}
        to={navigationItem.to}
        className={styles.navigationItem}
        activeClassName={styles.activeItem}
      >
        {navigationItem.label}
      </NavLink>
    ))}
    <a
      onClick={() => onLogout()}
      className={cn(styles.navigationItem, styles.logout)}
    >
      Logout
    </a>
  </div>
);

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
  onLogout: () => dispatch(logout()),
});

export default connect(null, mapDispatchToProps)(Navigation);
