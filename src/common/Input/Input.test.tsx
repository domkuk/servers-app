import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import Input, {Props} from './Input';

describe('Form', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      className: 'testClass',
      onInputChange: () => {},
      placeholder: 'test placeholder',
      name: 'test',
      onLoseFocus: () => {},
      type: 'password',
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render component correctly with validation error', () => {
    const component = getComponent({
      className: 'testClass',
      onInputChange: () => {},
      placeholder: 'test placeholder',
      name: 'test',
      onLoseFocus: () => {},
      type: 'password',
      validationErrors: ['Test error message'],
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should call event on input change', () => {
    const onInputChange = jest.fn();
    const component = shallow(getComponent({
      name: 'test',
      type: 'password',
      onLoseFocus: () => {},
      onInputChange,
    }));
    component.find('input').simulate('change');
    expect(onInputChange).toHaveBeenCalled();
  });

  it('should call event on input lose focus', () => {
    const onLoseFocus = jest.fn();
    const component = shallow(getComponent({
      name: 'test',
      type: 'password',
      onInputChange: () => {},
      onLoseFocus,
    }));
    component.find('input').simulate('blur');
    expect(onLoseFocus).toHaveBeenCalled();
  });
});

const getComponent = (props: Props) => (
  <Input {...props}/>
);
