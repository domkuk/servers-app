import React, {ChangeEvent} from 'react';
import styles from './Input.module.scss';
import cn from 'classnames';
import _ from 'lodash';

export type InputType = {
  className?: string,
  name: string,
  placeholder?: string,
  type: string,
  validationErrors?: Array<string | undefined>,
};

export type Props = InputType & {
  onInputChange: (event: ChangeEvent<HTMLInputElement>) => void,
  onLoseFocus: (event: ChangeEvent<HTMLInputElement>) => void,
}

const Input = ({
  className,
  name,
  placeholder,
  type,
  onInputChange,
  onLoseFocus,
  validationErrors,
}: Props) => (
  <div className={cn(styles.inputContainer, className)}>
    <div className={styles.input}>
      <input
        className={cn({[styles.error]: !_.isEmpty(validationErrors)})}
        name={name}
        placeholder={placeholder}
        type={type}
        onChange={onInputChange}
        onBlur={onLoseFocus}
      />
    </div>
    {!_.isEmpty(validationErrors) && (
      <div className={styles.errorsContainer}>
        {validationErrors && validationErrors.map((validationError) => (
          <div key={validationError}>{validationError}</div>
        ))}
      </div>
    )}
  </div>
);

Input.defaultProps = {
  onLoseFocus: () => {},
};

export default Input;
