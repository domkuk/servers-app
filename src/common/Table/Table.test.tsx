import React from 'react';
import renderer from 'react-test-renderer';
import Table, {Props} from './Table';
import {shallow} from 'enzyme';

describe('Table', () => {
  it('should render component correctly', () => {
    const component = getComponent({
      tableParams: {sortDirection: 'asc', sortBy: 'test'},
      className: 'testClass',
      headers: [{label: 'Test', sortBy: 'test'}, {label: 'foo'}],
      onSort: () => {},
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should call sort callback on header click', () => {
    const onSort = jest.fn();
    const component = shallow(getComponent({
      tableParams: {sortDirection: 'asc', sortBy: 'test'},
      className: 'testClass',
      headers: [{label: 'Test', sortBy: 'test'}, {label: 'foo'}],
      onSort,
    }));

    component.find('th').at(0).simulate('click');
    expect(onSort).toHaveBeenCalled();
  });

  it('should not call sort callback on header click without sort key', () => {
    const onSort = jest.fn();
    const component = shallow(getComponent({
      tableParams: {sortDirection: 'asc', sortBy: 'test'},
      className: 'testClass',
      headers: [{label: 'Test', sortBy: 'test'}, {label: 'foo'}],
      onSort,
    }));

    component.find('th').at(1).simulate('click');
    expect(onSort).not.toHaveBeenCalled();
  });
});

const getComponent = (props: Props) => (
  <Table {...props}/>
);
