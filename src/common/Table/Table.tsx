import React, {ReactNode} from 'react';
import styles from './Table.module.scss';
import cn from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSort, faSortDown, faSortUp} from '@fortawesome/free-solid-svg-icons';
import {
  SORT_ASC,
  SORT_DESC,
  SortBy,
  SortDirection,
  Table as TableType,
} from '../../hooks/useTable/useTable';

type Header = {
  label: string,
  sortBy?: string,
};

export type Props = {
  children?: ReactNode,
  className?: string,
  headers: Array<Header>,
  onSort: (sortBy: SortBy, sortDirection: SortDirection) => void,
  tableParams: TableType,
};

const Table = ({children, className, headers, onSort, tableParams}: Props) => {
  const handleHeaderClick = (clickedSortBy: SortBy) => {
    if (clickedSortBy !== tableParams.sortBy) {
      onSort(clickedSortBy, SORT_ASC);
      return;
    }

    if (tableParams.sortDirection === SORT_ASC) {
      onSort(clickedSortBy, SORT_DESC);
      return;
    }

    onSort(null, SORT_ASC);
  };

  return (
    <table className={cn(styles.table, className)}>
      {headers.length > 0 && (
        <thead>
          <tr>
            {headers.map((header) => (
              <th
                className={cn({[styles.sortable]: header.sortBy})}
                key={header.label.toString()}
                onClick={
                  () => header.sortBy && handleHeaderClick(header.sortBy)
                }
              >
                {header.label}
                {header.sortBy && (
                  <FontAwesomeIcon
                    fixedWidth
                    icon={
                      tableParams.sortBy !== header.sortBy ? faSort :
                        tableParams.sortDirection === SORT_ASC ? faSortDown :
                          faSortUp
                    }/>
                )}
              </th>
            ))}
          </tr>
        </thead>
      )}
      <tbody>
        {children}
      </tbody>
    </table>
  );
};

Table.defaultProps = {
  headers: [],
  onSort: () => {},
};

export default Table;
