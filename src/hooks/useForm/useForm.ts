import {ChangeEvent, useState} from 'react';
import {
  getValidationError,
  ValidationRule,
} from '../../utility/validation/validation';
import _ from 'lodash';

export type FormInputBlueprint = {
  name: string,
  type: string,
  validation?: Array<ValidationRule>,
}

export type FormInput = FormInputBlueprint & {
  isValidatable?: boolean,
  value?: string,
  validationErrors?: Array<string>,
};

export type FormSubmitInput = {
  [key: string]: string,
};

export const useForm = <T>(
  inputBlueprints: Array<FormInputBlueprint>,
  onFormSubmit: (inputs: T) => void,
) => {
  const [inputs, setInputs] = useState<Array<FormInput>>(inputBlueprints);

  const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();

    setInputs((prevState) => prevState.map((prevInput) => (
      prevInput.name === event.target.name ? {
        ...prevInput,
        value: event.target.value,
        validationErrors:
          prevInput.isValidatable ? getValidationError(
            event.target.value, prevInput.validation,
          ) : [],
      } : {...prevInput}
    )));
  };

  const onLoseInputFocus = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();

    setInputs((prevState) => prevState.map((prevInput) => (
      prevInput.name === event.target.name ? {
        ...prevInput,
        isValidatable: true,
        validationErrors: getValidationError(
          prevInput.value || '', prevInput.validation,
        ),
      } : {...prevInput}
    )));
  };

  const onSubmit = (event: ChangeEvent) => {
    event.preventDefault();

    const validatedInputs = inputs.map((input) => ({
      ...input,
      validationErrors: getValidationError(
        input.value || '', input.validation,
      ),
    }));

    if (_.find(
      validatedInputs,
      (validatedInput) => !_.isEmpty(validatedInput.validationErrors))
    ) {
      setInputs(validatedInputs);
      return;
    }

    onFormSubmit(
      Object.assign(
        {},
        ...inputs.map((input) => ({[input.name]: input.value}))),
    );
  };

  return {inputs, onInputChange, onLoseInputFocus, onSubmit};
};
