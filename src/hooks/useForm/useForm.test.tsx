import React from 'react';
import {renderHook, act} from '@testing-library/react-hooks';
import {FormInputBlueprint, useForm} from './useForm';

const mockInputBluePrint: FormInputBlueprint = {
  name: 'test',
  type: 'password',
};

describe('useForm', () => {
  it('should change inputs value', () => {
    const onFormSubmit = jest.fn();
    const {result} = renderHook(
      () => useForm([mockInputBluePrint], onFormSubmit));

    act(() => {
      const event = {
        preventDefault: () => {},
        target: {
          name: 'test',
          value: 'testing',
        },
      } as React.ChangeEvent<HTMLInputElement>;
      result.current.onInputChange(event);
    });

    expect(result.current.inputs)
      .toStrictEqual([{
        name: 'test',
        type: 'password',
        value: 'testing',
        validationErrors: [],
      }]);
  });

  it('should validate input on blur', () => {
    const onFormSubmit = jest.fn();
    const {result} = renderHook(
      () => useForm([{
        ...mockInputBluePrint,
        validation: [{type: 'required'}],
      }], onFormSubmit));

    const event = {
      preventDefault: () => {},
      target: {
        name: 'test',
        value: '',
      },
    } as React.ChangeEvent<HTMLInputElement>;

    act(() => {
      result.current.onInputChange(event);
    });

    act(() => {
      result.current.onLoseInputFocus(event);
    });

    expect(result.current.inputs)
      .toStrictEqual([{
        name: 'test',
        type: 'password',
        value: '',
        validationErrors: ['This field cannot be empty'],
        validation: [{type: 'required'}],
        isValidatable: true,
      }]);
  });

  it('should call submit callback on submit', () => {
    const onFormSubmit = jest.fn();
    const {result} = renderHook(
      () => useForm([mockInputBluePrint], onFormSubmit));

    act(() => {
      const event = {
        preventDefault: () => {},
      } as React.ChangeEvent;
      result.current.onSubmit(event);
    });

    expect(onFormSubmit).toHaveBeenCalled();
  });
});
