import {useEffect, useState} from 'react';

export const SORT_ASC = 'asc';
export const SORT_DESC = 'desc';

export type SortDirection = typeof SORT_ASC | typeof SORT_DESC;
export type SortBy = string | null;

export type Table = {
  sortBy: SortBy,
  sortDirection: SortDirection,
}

export const useTable = (onUpdate: (tableParams: Table) => void) => {
  const [tableParams, setTableParams] = useState<Table>({
    sortBy: null,
    sortDirection: SORT_ASC,
  });

  useEffect(() => {
    onUpdate(tableParams);
  }, [tableParams]);

  const onSort = (sortBy: SortBy, sortDirection: SortDirection) =>
    setTableParams((prevState) => ({
      ...prevState,
      sortBy,
      sortDirection,
    }));

  return {tableParams, onSort};
};
