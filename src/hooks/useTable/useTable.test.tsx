import {renderHook, act} from '@testing-library/react-hooks';
import {useTable} from './useTable';

describe('useTable', () => {
  it('should call callback on sort', () => {
    const onUpdate = jest.fn();
    const {result} = renderHook(
      () => useTable(onUpdate));

    act(() => {
      result.current.onSort('test', 'asc');
    });

    expect(result.current.tableParams)
      .toEqual({
        sortBy: 'test',
        sortDirection: 'asc',
      });
    expect(onUpdate).toHaveBeenCalled();
  });
});
