import * as actionTypes from './actionTypes';
import {Server} from '../../domain/Server';

export const fetchServersStart = () => ({
  type: actionTypes.FETCH_SERVERS_START,
});

export const fetchServersSuccess = (servers: Array<Server>) => ({
  type: actionTypes.FETCH_SERVERS_SUCCESS,
  servers,
});

export const fetchServersFail = (serversError: string) => ({
  type: actionTypes.FETCH_SERVERS_FAIL,
  serversError,
});
