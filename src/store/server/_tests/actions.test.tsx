import * as actionTypes from '../actionTypes';
import {
  fetchServersStart,
  fetchServersSuccess,
  fetchServersFail,
} from '../actions';
import {servers} from '../../../__mocks/server';

describe('serverActions', () => {
  it('should create a server fetch start action', () => {
    const expectedAction = {
      type: actionTypes.FETCH_SERVERS_START,
    };
    expect(fetchServersStart()).toEqual(expectedAction);
  });

  it('should create a server fetch success action', () => {
    const expectedAction = {
      type: actionTypes.FETCH_SERVERS_SUCCESS,
      servers,
    };
    expect(fetchServersSuccess(servers)).toEqual(expectedAction);
  });

  it('should create a server fetch failure action', () => {
    const expectedAction = {
      type: actionTypes.FETCH_SERVERS_FAIL,
      serversError: 'failed',
    };
    expect(fetchServersFail('failed')).toEqual(expectedAction);
  });
});
