import reducer, {ServerActionType} from '../reducer';
import {servers} from '../../../__mocks/server';

describe('serverReducer', () => {
  it('should return state after fetch servers start', () => {
    const action = {
      type: 'FETCH_SERVERS_START',
    } as ServerActionType;
    expect(reducer(undefined, action)).toEqual({
      servers: [],
      serversLoading: true,
      serversError: null,
    });
  });

  it('should return state after fetch servers success', () => {
    const action = {
      type: 'FETCH_SERVERS_SUCCESS',
      servers,
    } as ServerActionType;
    expect(reducer(undefined, action)).toEqual({
      serversLoading: false,
      serversError: null,
      servers,
    });
  });

  it('should return state after fetch servers failure', () => {
    const action = {
      type: 'FETCH_SERVERS_FAIL',
      serversError: 'failed test',
    } as ServerActionType;
    expect(reducer(undefined, action)).toEqual({
      serversLoading: false,
      serversError: 'failed test',
      servers: [],
    });
  });
});
