import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from '../../../config/Axios/axios-instance';
import MockAdapter from 'axios-mock-adapter';
import * as serverService from '../service';
import {servers} from '../../../__mocks/server';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const mock = new MockAdapter(axios);
const store = mockStore();

describe('serverService', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('should dispatch start and success actions when success', () => {
    mock.onGet('/servers')
      .reply(200, servers);

    store.dispatch<any>(serverService.fetchServers()).then(() => {
      const expectedActions = [
        {type: 'FETCH_SERVERS_START'},
        {type: 'FETCH_SERVERS_SUCCESS', servers},
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should dispatch start and fail actions when failed', () => {
    mock.onGet('/servers')
      .reply(500, 'failed');

    store.dispatch<any>(serverService.fetchServers()).then(() => {
      const expectedActions = [
        {type: 'FETCH_SERVERS_START'},
        {type: 'FETCH_SERVERS_FAIL'},
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
