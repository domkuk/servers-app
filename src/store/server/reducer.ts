import * as actionTypes from './actionTypes';
import {ServerActionTypes} from './actionTypes';
import {Server} from '../../domain/Server';

export type ServerStateType = {
  servers: Array<Server>,
  serversLoading: boolean,
  serversError: string | null,
};

export type ServerActionType = ServerStateType & {
  type: ServerActionTypes,
};

export const initialState: ServerStateType = {
  servers: [],
  serversLoading: false,
  serversError: null,
};

const fetchServersStart = (state: ServerStateType): ServerStateType => ({
  ...state,
  serversLoading: true,
});

const fetchServersSuccess = (
  state: ServerStateType, action: ServerActionType,
): ServerStateType => ({
  ...state,
  servers: action.servers,
  serversLoading: false,
  serversError: null,
});

const fetchServersFail = (
  state: ServerStateType, action: ServerActionType,
): ServerStateType => ({
  ...state,
  serversError: action.serversError,
  serversLoading: false,
});

const reducer = (state = initialState, action: ServerActionType) => {
  switch (action.type) {
  case actionTypes.FETCH_SERVERS_START:
    return fetchServersStart(state);
  case actionTypes.FETCH_SERVERS_SUCCESS:
    return fetchServersSuccess(state, action);
  case actionTypes.FETCH_SERVERS_FAIL:
    return fetchServersFail(state, action);
  default:
    return state;
  }
};

export default reducer;
