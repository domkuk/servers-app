import axios from '../../config/Axios/axios-instance';
import {
  fetchServersFail,
  fetchServersStart,
  fetchServersSuccess,
} from './actions';
import {Dispatch} from 'redux';

const API_URL = '/servers';

export const fetchServers = () => (dispatch: Dispatch) => {
  dispatch(fetchServersStart());
  return axios.get(API_URL)
    .then((response) => {
      dispatch(fetchServersSuccess(response.data));
    })
    .catch((err) => {
      dispatch(fetchServersFail(err.response.data.error));
    })
  ;
};
