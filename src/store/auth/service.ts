import axios from '../../config/Axios/axios-instance';
import {loginFail, loginStart, loginSuccess, logout} from './actions';
import {Dispatch} from 'redux';

const API_URL = '/tokens';

export type LoginRequest = {
  username: string,
  password: string,
};

export const login =
  (inputs: LoginRequest) => (dispatch: Dispatch) => {
    dispatch(loginStart());
    return axios.post(API_URL, {
      ...inputs,
    })
      .then((response) => {
        localStorage.setItem('token', response.data.token);
        dispatch(loginSuccess());
      })
      .catch((err) => {
        dispatch(loginFail(err.response.data.message));
      })
    ;
  };

export const authCheckState = () => (dispatch: Dispatch) => {
  const token = localStorage.getItem('token');
  return token ? dispatch(loginSuccess()) : dispatch(logout());
};
