import * as actionTypes from './actionTypes';
import {AuthActionTypes} from './actionTypes';

export type AuthStateType = {
  isAuthenticated: boolean,
  isInitCompleted: boolean,
  loginLoading: boolean,
  loginError: string | null,
};

export type AuthActionType = AuthStateType & {
  type: AuthActionTypes,
};

export const initialState: AuthStateType = {
  isAuthenticated: false,
  isInitCompleted: false,
  loginLoading: false,
  loginError: null,
};

const loginStart = (state: AuthStateType): AuthStateType => ({
  ...state,
  loginLoading: true,
});

const loginSuccess = (state: AuthStateType): AuthStateType => ({
  ...state,
  isAuthenticated: true,
  loginError: null,
  loginLoading: false,
  isInitCompleted: true,
});

const loginFail = (
  state: AuthStateType, action: AuthActionType,
): AuthStateType => ({
  ...state,
  loginError: action.loginError,
  loginLoading: false,
  isInitCompleted: true,
});

const logout = (): AuthStateType => ({
  isAuthenticated: false,
  loginLoading: false,
  loginError: null,
  isInitCompleted: true,
});

const reducer = (state = initialState, action: AuthActionType) => {
  switch (action.type) {
  case actionTypes.LOGIN_START:
    return loginStart(state);
  case actionTypes.LOGIN_SUCCESS:
    return loginSuccess(state);
  case actionTypes.LOGIN_FAIL:
    return loginFail(state, action);
  case actionTypes.LOGOUT:
    return logout();
  default:
    return state;
  }
};

export default reducer;
