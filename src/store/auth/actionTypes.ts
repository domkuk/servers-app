export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT = 'LOGOUT';

export type AuthActionTypes =
  typeof LOGIN_START |
  typeof LOGIN_SUCCESS |
  typeof LOGIN_FAIL |
  typeof LOGOUT
;
