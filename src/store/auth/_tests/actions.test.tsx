import * as actionTypes from '../actionTypes';
import {loginFail, loginStart, loginSuccess, logout} from '../actions';

describe('authActions', () => {
  it('should create a login start action', () => {
    const expectedAction = {
      type: actionTypes.LOGIN_START,
    };
    expect(loginStart()).toEqual(expectedAction);
  });

  it('should create a login success action', () => {
    const expectedAction = {
      type: actionTypes.LOGIN_SUCCESS,
    };
    expect(loginSuccess()).toEqual(expectedAction);
  });

  it('should create a login failure action', () => {
    const expectedAction = {
      type: actionTypes.LOGIN_FAIL,
      loginError: 'failed',
    };
    expect(loginFail('failed')).toEqual(expectedAction);
  });

  it('should create a logout action', () => {
    const expectedAction = {
      type: actionTypes.LOGOUT,
    };
    expect(logout()).toEqual(expectedAction);
  });
});
