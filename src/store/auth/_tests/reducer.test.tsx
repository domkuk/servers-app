import reducer, {AuthActionType} from '../reducer';

describe('authReducer', () => {
  it('should return state after login start', () => {
    const action = {
      isAuthenticated: false,
      loginLoading: false,
      loginError: null,
      type: 'LOGIN_START',
    } as AuthActionType;
    expect(reducer(undefined, action)).toEqual({
      isAuthenticated: false,
      isInitCompleted: false,
      loginError: null,
      loginLoading: true,
    });
  });

  it('should return state after login success', () => {
    const action = {
      isAuthenticated: false,
      loginLoading: false,
      loginError: null,
      type: 'LOGIN_SUCCESS',
    } as AuthActionType;
    expect(reducer(undefined, action)).toEqual({
      isAuthenticated: true,
      loginError: null,
      loginLoading: false,
      isInitCompleted: true,
    });
  });

  it('should return state after login failure', () => {
    const action = {
      isAuthenticated: false,
      loginLoading: false,
      loginError: 'failed test',
      type: 'LOGIN_FAIL',
    } as AuthActionType;
    expect(reducer(undefined, action)).toEqual({
      isAuthenticated: false,
      loginError: 'failed test',
      loginLoading: false,
      isInitCompleted: true,
    });
  });

  it('should return state after logout', () => {
    const action = {
      isAuthenticated: false,
      loginLoading: false,
      loginError: null,
      type: 'LOGOUT',
    } as AuthActionType;
    expect(reducer(undefined, action)).toEqual({
      isAuthenticated: false,
      loginError: null,
      loginLoading: false,
      isInitCompleted: true,
    });
  });
});
