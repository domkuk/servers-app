import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from '../../../config/Axios/axios-instance';
import MockAdapter from 'axios-mock-adapter';
import * as authService from '../service';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const mock = new MockAdapter(axios);
const store = mockStore();

describe('authService', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('should dispatch start and success actions when success', () => {
    mock.onPost('/tokens')
      .reply(200, {response: {token: 'test123'}});

    const inputs = {
      username: 'test_user',
      password: 'test_password',
    };
    store.dispatch<any>(authService.login(inputs)).then(() => {
      const expectedActions = [
        {type: 'LOGIN_START'},
        {type: 'LOGIN_SUCCESS'},
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should dispatch start and fail actions when failed', () => {
    mock.onPost('/tokens')
      .reply(500, 'failed');

    const inputs = {
      username: 'test_user',
      password: 'test_password',
    };
    store.dispatch<any>(authService.login(inputs)).then(() => {
      const expectedActions = [
        {type: 'LOGIN_START'},
        {type: 'LOGIN_FAIL'},
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
