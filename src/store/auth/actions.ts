import * as actionTypes from './actionTypes';

export const loginStart = () => ({
  type: actionTypes.LOGIN_START,
});

export const loginSuccess = () => ({
  type: actionTypes.LOGIN_SUCCESS,
});

export const loginFail = (loginError: string) => ({
  type: actionTypes.LOGIN_FAIL,
  loginError,
});

export const logout = () => {
  localStorage.removeItem('token');
  return {
    type: actionTypes.LOGOUT,
  };
};
