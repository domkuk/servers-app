import React from 'react';
import StoreProvider from './config/StoreProvider/StoreProvider';
import Router from './config/Router/Router';

const App = () => (
  <StoreProvider>
    <Router/>
  </StoreProvider>
);

export default App;
