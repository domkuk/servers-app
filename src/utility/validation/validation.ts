import _ from 'lodash';

const MESSAGE_REQUIRED = 'This field cannot be empty';

export type ValidationRule = {
  type: string,
  value?: string | number,
}

export const getValidationError = (
  value: string,
  rules: Array<ValidationRule> | undefined,
): Array<string> => _.compact(_.map(rules, (rule) => {
  const validator = validatorFactory(rule);
  const validationValue = validator && validator(value);
  if (validationValue) {
    return validationValue;
  }
}));

const validatorFactory = (rule: ValidationRule) => {
  switch (rule.type) {
  case 'required':
    return requiredValidator;
  }
};

const requiredValidator = (value: string): string | undefined => (
  value.length === 0 ? MESSAGE_REQUIRED : undefined
);
