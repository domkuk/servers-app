import {getValidationError} from './validation';

describe('validation', () => {
  it('should return error based on required rule', () => {
    expect(getValidationError('', [{type: 'required'}]))
      .toStrictEqual(['This field cannot be empty']);
  });

  it('should return empty list of errors when valid', () => {
    expect(getValidationError('test', [{type: 'required'}]))
      .toStrictEqual([]);
  });
});
