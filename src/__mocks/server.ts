import {Server} from '../domain/Server';

export const servers: Array<Server> = [
  {
    name: 'Test 1',
    distance: 100,
  },
  {
    name: 'Test 2',
    distance: 500,
  },
];
