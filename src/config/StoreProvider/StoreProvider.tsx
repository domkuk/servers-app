import React, {ReactNode} from 'react';
import {Provider} from 'react-redux';
import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
  Store,
} from 'redux';
import thunk from 'redux-thunk';
import authReducer, {AuthStateType} from '../../store/auth/reducer';
import serverReducer, {ServerStateType} from '../../store/server/reducer';
import instance from '../Axios/axios-instance';
import * as authService from '../../store/auth/actions';

export type StoreState = {
  auth: AuthStateType,
  server: ServerStateType,
};

type Props = {
  children?: ReactNode,
};

const rootReducer = combineReducers<StoreState>({
  auth: authReducer,
  server: serverReducer,
});

export const composeEnhancers =
  (window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const store: Store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk),
));

const {dispatch} = store;
instance.interceptors.response.use((response) => response, (error) => {
  if (error.response.status === 401 &&
    error.response.config.url !== '/tokens') {
    dispatch(authService.logout());
  }

  return Promise.reject(error);
});

const StoreProvider = ({children}: Props) => (
  <Provider store={store}>
    {children}
  </Provider>
);

export default StoreProvider;
