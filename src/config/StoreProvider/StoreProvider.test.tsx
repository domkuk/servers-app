import React from 'react';
import renderer from 'react-test-renderer';
import StoreProvider from './StoreProvider';

describe('StoreProvider', () => {
  it('should render component correctly', () => {
    const component = getComponent();
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = () => (
  <StoreProvider>
    Test
  </StoreProvider>
);
