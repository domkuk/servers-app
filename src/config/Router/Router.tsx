import React, {Suspense, useEffect} from 'react';
import {BrowserRouter, Switch, Redirect, Route} from 'react-router-dom';
import Layout from '../../common/Layout/Layout';
import {StoreState} from '../StoreProvider/StoreProvider';
import {connect} from 'react-redux';
import {AnyAction} from 'redux';
import * as authService from '../../store/auth/service';
import {ThunkDispatch} from 'redux-thunk';
import Loader from '../../common/Loader/Loader';

const Login = React.lazy(() => import('../../pages/Login/Login'));
const Servers = React.lazy(() => import('../../pages/Servers/Servers'));

export type Props = {
  isAuthenticated: boolean,
  isInitCompleted: boolean,
  onTryAutoSignup: () => void,
};

export const Router = ({
  isAuthenticated,
  isInitCompleted,
  onTryAutoSignup,
}: Props) => {
  useEffect(() => {
    onTryAutoSignup();
  }, [onTryAutoSignup]);

  return (
    <BrowserRouter>
      <Layout isAuthenticated={isAuthenticated}>
        {isInitCompleted ? (
          <Suspense fallback={<Loader isLoading isFullScreen/>}>
            {isAuthenticated ? (
              <Switch>
                <Route path='/servers' exact component={Servers}/>
                <Redirect to='/servers'/>
              </Switch>
            ) : (
              <Switch>
                <Route path='/login' exact component={Login}/>
                <Redirect to='/login'/>
              </Switch>
            )}
          </Suspense>
        ) : (
          <Loader isLoading isFullScreen/>
        )}
      </Layout>
    </BrowserRouter>
  );
};

const mapStateToProps = (state: StoreState) => ({
  isAuthenticated: state.auth.isAuthenticated,
  isInitCompleted: state.auth.isInitCompleted,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
  onTryAutoSignup: () => dispatch(authService.authCheckState()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Router);
