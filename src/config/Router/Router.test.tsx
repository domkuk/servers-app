import React from 'react';
import renderer from 'react-test-renderer';
import {Router, Props} from './Router';

jest.mock('../../components/Login/LoginForm/LoginForm', () => 'LoginForm');
jest.mock(
  '../../components/Server/ServersTable/ServersTable', () => 'ServersTable');
jest.mock('../../common/Navigation/Navigation', () => 'Navigation');

describe('Router', () => {
  it('should render loading component when authenticated', () => {
    const component = getComponent({
      isAuthenticated: true,
      isInitCompleted: true,
      onTryAutoSignup: () => {},
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render loading component when not authenticated', () => {
    const component = getComponent({
      isAuthenticated: false,
      isInitCompleted: true,
      onTryAutoSignup: () => {},
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });

  it('should render loading component when not init completed', () => {
    const component = getComponent({
      isAuthenticated: true,
      isInitCompleted: false,
      onTryAutoSignup: () => {},
    });
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = (props: Props) => (
  <Router {...props}/>
);
