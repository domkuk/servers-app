import React from 'react';
import renderer from 'react-test-renderer';
import Login from './Login';

jest.mock('../../components/Login/LoginForm/LoginForm', () => 'LoginForm');

describe('Login', () => {
  it('should render component correctly', () => {
    const component = getComponent();
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = () => (
  <Login/>
);
