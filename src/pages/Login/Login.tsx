import React from 'react';
import LoginForm from '../../components/Login/LoginForm/LoginForm';
import styles from './Login.module.scss';

const Login = () => (
  <div className={styles.loginPage}>
    <div className={styles.loginFormContainer}>
      <div className={styles.title}>Login</div>
      <LoginForm/>
    </div>
  </div>
);

export default Login;
