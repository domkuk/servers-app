import React from 'react';
import ServersTable from '../../components/Server/ServersTable/ServersTable';
import styles from './Servers.module.scss';

const Servers = () => (
  <div className={styles.servers}>
    <div className={styles.serversListContainer}>
      <div className={styles.serversList}>
        <div className={styles.title}>
          Servers list
        </div>
        <ServersTable/>
      </div>
    </div>
  </div>
);

export default Servers;
