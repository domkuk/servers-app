import React from 'react';
import renderer from 'react-test-renderer';
import Servers from './Servers';

jest.mock(
  '../../components/Server/ServersTable/ServersTable', () => 'ServersTable');

describe('Servers', () => {
  it('should render component correctly', () => {
    const component = getComponent();
    expect(renderer.create(component).toJSON()).toMatchSnapshot();
  });
});

const getComponent = () => (
  <Servers/>
);
