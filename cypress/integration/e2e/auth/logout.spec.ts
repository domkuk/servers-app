import 'cypress-localstorage-commands';

describe('Logout', () => {
  beforeEach(() => {
    cy.setLocalStorage('token', 'valid_token');
    cy.visit('http://localhost:3000/servers');
  });

  afterEach(() => {
    cy.clearLocalStorage();
  });

  it('should logout user on logout click', () => {
    cy.get('a').eq(1).click();

    cy.url().should('include', '/login');
  });

  it('should logout user on expired token', () => {
    cy.intercept('/servers', {statusCode: 401});

    cy.url().should('include', '/login');
  });
});

export default {};
