describe('Login page', () => {
  beforeEach(() => {
    cy.clearLocalStorage();
    cy.visit('http://localhost:3000');
  });

  it('should login using correct credentials', () => {
    cy.intercept('/tokens', {fixture: 'token', statusCode: 200});

    cy.get('input[name="username"]')
      .type('admin')
      .should('have.value', 'admin');

    cy.get('input[name="password"]')
      .type('password')
      .should('have.value', 'password');

    cy.get('button').contains('Login').click();

    cy.url().should('include', '/servers');
  });

  it('should not login with bad credentials', () => {
    cy.intercept('/tokens', {statusCode: 500, body: {message: 'Test error'}});

    cy.get('input[name="username"]')
      .type('admin')
      .should('have.value', 'admin');

    cy.get('input[name="password"]')
      .type('password')
      .should('have.value', 'password');

    cy.get('button').contains('Login').click();

    cy.contains('Test error');

    cy.url().should('include', '/login');
  });
});

export default {};
