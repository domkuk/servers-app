import 'cypress-localstorage-commands';

describe('Server page', () => {
  beforeEach(() => {
    cy.setLocalStorage('token', 'valid_token');
    cy.visit('http://localhost:3000/servers');
  });

  afterEach(() => {
    cy.clearLocalStorage();
  });

  it('should see servers list', () => {
    cy.intercept('/servers', {fixture: 'server', statusCode: 200});

    cy.url().should('include', '/servers');

    cy.get('table').contains('Test 1');
    cy.get('table').contains('Test 3');
    cy.get('table').contains('Test 2');
  });

  it('should sort servers table by name', () => {
    cy.intercept('/servers', {fixture: 'server', statusCode: 200});

    cy.url().should('include', '/servers');

    cy.get('thead').get('th').eq(0).click();

    cy.get('tr').eq(1).contains('Test 1');
    cy.get('tr').eq(2).contains('Test 2');
    cy.get('tr').eq(3).contains('Test 3');

    cy.get('thead').get('th').eq(0).click();

    cy.get('tr').eq(1).contains('Test 3');
    cy.get('tr').eq(2).contains('Test 2');
    cy.get('tr').eq(3).contains('Test 1');

    cy.get('thead').get('th').eq(0).click();

    cy.get('tr').eq(1).contains('Test 1');
    cy.get('tr').eq(2).contains('Test 3');
    cy.get('tr').eq(3).contains('Test 2');
  });

  it('should sort servers table by distance', () => {
    cy.intercept('/servers', {fixture: 'server', statusCode: 200});

    cy.url().should('include', '/servers');

    cy.get('thead').get('th').eq(1).click();

    cy.get('tr').eq(1).contains('Test 2');
    cy.get('tr').eq(2).contains('Test 1');
    cy.get('tr').eq(3).contains('Test 3');

    cy.get('thead').get('th').eq(1).click();

    cy.get('tr').eq(1).contains('Test 3');
    cy.get('tr').eq(2).contains('Test 1');
    cy.get('tr').eq(3).contains('Test 2');

    cy.get('thead').get('th').eq(1).click();

    cy.get('tr').eq(1).contains('Test 1');
    cy.get('tr').eq(2).contains('Test 3');
    cy.get('tr').eq(3).contains('Test 2');
  });
});

export default {};
